libmoosex-runnable-perl (0.10-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper dependency to >= 9, since that's what is used in
    debian/compat.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.
  * Update standards version to 4.4.1, no changes needed.
  * Update standards version to 4.5.0, no changes needed.
  * Remove constraints unnecessary since stretch:
    + Build-Depends: Drop versioned constraint on perl.
  * Update standards version to 4.5.1, no changes needed.
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 11 Dec 2022 18:48:33 +0000

libmoosex-runnable-perl (0.10-1) unstable; urgency=medium

  [ upstream ]
  * New release.

  [ gregor herrmann ]
  * Update Test::use::ok build dependency.
  * Update URLs from {search,www}.cpan.org to MetaCPAN.
  * Update GitHub URLs to use HTTPS.

  [ Jonas Smedegaard ]
  * Update copyright info:
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
    + Fix use double space between copyright holders.
    + Extend coverage of packaging to include recent years.
    + Use https protocol in Format URL.
  * Add lintian override regarding license in License-Reference field.
    See bug#786450.
  * Bump debhelper compatibility level to 9.
  * Declare compliance with Debian Policy 4.3.0.
  * Update watch file:
    + Bump to file format 4.
    + Drop secondary URL.
    + Mention gbp --uscan in usage comment.
    + Use substitution strings.
  * Simplify rules.
    Stop build-depend on licensecheck cdbs.
  * Stop build-depend on dh-buildinfo.
  * Wrap and sort control file.
  * Mark build-dependencies needed only for testsuite as such.
  * Enable autopkgtest.
  * Set Rules-Requires-Root: no.
  * Update git-buildpackage config: Filter any .git* file.
  * (Build-)depend on liblist-someutils-perl
    (not liblist-moreutils-perl).
  * Remove empty manpages.

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 20 Feb 2019 15:18:18 +0100

libmoosex-runnable-perl (0.09-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).
    + Fix untranslated shebang in mx-run.
    + Remove README.pod from shipped dist.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to use cgit web frontend.

  [ Jonas Smedegaard ]
  * Update watch file to use metacpan.org and www.cpan.org URLs
    (replacing search.cpan.org URL).
  * Update package relations:
    + Fix include perl-build.mk (not deprecated perl-module.mk).
    + Tighten build-dependency on cdbs: Needed for detecting use of
      Module::Build::Tiny.
  * Update copyright info:
    + Add alternate git source.
  * Declare compliance with Debian Policy 3.9.6.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 14 Oct 2014 14:23:44 +0200

libmoosex-runnable-perl (0.07-1) unstable; urgency=medium

  * Imported Upstream version 0.07 (Closes: #741413)
  * Update DEB_UPSTREAM_TARBALL_MD5
  * Add myself to uploaders
  * Bump standards version to 3.9.5
  * Use canonical URL for VCS-Git
  * Versioned build-depend for debhelper 8
  * Add versioned build-depends for cdbs/debhelper due to
    Module::Build::Tiny usage
  * debian/copyright - Drop inc/* copyright stanza
  * Update debian/rules with new build-deps and suggests
  * Regenerate debian/control

 -- Daniel Lintott <daniel@serverb.co.uk>  Sat, 12 Apr 2014 15:29:52 +0100

libmoosex-runnable-perl (0.03-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#706775.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 04 May 2013 21:31:00 +0200
